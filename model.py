"""model.py - Model do padrão MVC para aplicações desktop"""

from observer import Observable
from exceptions import *


class Calculator(Observable):
    def __init__(self):
        """
            Construct a new 'Calculator' object. Business logic

            :param: nothing
            :return: returns nothing
        """
        super().__init__()

    def compute(self, expression):
        """
            Evaluates an expression and notifies the result to all subscribed observers

            :param expression: string to be evaluated
            :return: returns a number
        """
        try:
            result = eval(expression)
        except ZeroDivisionError as e:
            raise DivisionByZeroException
        except Exception as e:
            raise InvalidExpressionException

        self.notify({'success': True, 'expression': expression, 'result': result})
