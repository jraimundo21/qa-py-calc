"""exceptions.py - Criação de excepções customizadas"""

import json

with open('message_dictionary.json', encoding='utf-8') as f:
    my_dictionary = json.load(f)["pt"]["exceptions"]


class InvalidExpressionException(Exception):
    def __init__(self, message=my_dictionary["invalidExpression"]):
        """
            Construct a new 'InvalidExpressionException' object.

            :param message: message to display on exception, defaulted in json file
            :return: returns nothing
        """
        self.message = message
        super().__init__(self.message)


class DivisionByZeroException(Exception):
    """
        Construct a new 'DivisionByZeroException' object.

        :param message: message to display on exception, defaulted in json file
        :return: returns nothing
    """
    def __init__(self, message=my_dictionary["divisionByZero"]):
        self.message = message
        super().__init__(self.message)
