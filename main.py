"""main.py Entry point para a calculadora"""

import sys

import model
import view_cli
import view_gui
from controller import Controller


def main():
    """
        Software entry point

        :param: nothing
        :return: returns string
    """

    # Create model instance
    my_model = model.Calculator()

    # Create command line user interface instance, for logging
    cli = view_cli.Cli()

    # Create graphical user interface instance
    gui = view_gui.Gui()

    # The looping interface must be the last
    my_views = [cli, gui]

    # Create and launch application controller
    my_controller = Controller(my_model, my_views)
    my_controller.launch()


if __name__ == "__main__":
    sys.exit(main())
