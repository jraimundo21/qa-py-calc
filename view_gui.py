"""view_gui.py - View para interface gráfica do padrão MVC para aplicações desktop"""

from tkinter import *
from tkinter import messagebox
import json
from view_interface import CalcViewInterface

with open('message_dictionary.json', encoding='utf-8') as f:
    ui_names = json.load(f)["pt"]["ui_names"]


class Gui(CalcViewInterface):
    def __init__(self):
        """
            Construct a new 'Gui' object.

            :param: no paramsS
            :return: returns nothing
        """
        super().__init__('Gui')
        # Class variables
        self._LARGE_FONT = ('Verdana', 20)
        self._SMALL_FONT = ('Verdana', 10, 'bold')
        self._ENTRY_POINT = ('Lucida Console', 20)
        self._DEFAULT_EMPTY_INPUT = ui_names["empty_input"]
        self._DEFAULT_BUTTON_PROPS = {'text': '', 'command': None,
                                      'fg': 'black', 'bg': 'gray',
                                      'height': 2, 'width': 10,
                                      'row': 0, 'column': 0,
                                      'columnspan': 1, 'rowspan': 1,
                                      'font': self._SMALL_FONT}
        self._input_value = None
        self._output_value = None
        self.trigger_event = None

    # Method override
    def set_triggers(self, controller):
        """
            Configures controller computation action

            :param: controller instance
            :return: returns nothing
        """
        self.trigger_event = controller.compute_result

    # Method override
    def initialize(self):
        """
            Initialize the GUI window

            :param: none
            :return: returns nothing
        """
        self.create_gui()

    # Method override
    def update(self, message):
        """
            Observer method override: Sets the output box with the result from the model(Observable)

            :param message: {"success": bool, "expression": string, "result": string}: Object from Observable object.
            :return: returns nothing
        """
        if message['success']:
            self._output_value.set(self.remove_zero_on_the_right(message['result']))
            self._input_value.set(message['expression'])
        elif message['error']:
            self.show_error(message['error_title'], message['error_message'])

    @classmethod
    def show_error(cls, error_title, error_message):
        """
            Shows message box with the error_message

            :param: error_message
            :return: returns nothing
        """
        messagebox.showinfo(error_title, error_message)

    @classmethod
    def remove_zero_on_the_right(cls, value):
        """
            Removes the zero on the right on any float, double or string (remove .0)

            :param value: Target value, float, double or string
            :return: returns string
        """
        value = str(value)
        if value[-2:] == '.0':
            return value[:(len(value) - 2)]
        return value

    @classmethod
    def represents_int(cls, string):
        """
            Checks

            :param string: a string
            :return: returns bool: True if string represents an integer
        """
        try:
            int(string)
            return True
        except ValueError:
            return False

    def press(self, char, run=False):
        """
            Add character to expression in input box, and computes result on real-time

            :param char: Character to present on the input box
            :param run: bool: if true, updates output box on the fly
            :return: returns nothing
        """
        if len(self._input_value.get()) > 0 and self._input_value.get() == self._DEFAULT_EMPTY_INPUT:
            self._input_value.set('')
        self._input_value.set(self._input_value.get() + '' + str(char))
        if run and (self._input_value.get().count('(') == self._input_value.get().count(')')):
            self.trigger_event(self._input_value.get())

    def save_result(self):
        """
            Saves result (output) to the input

            :param: nothing
            :return: nothing
        """
        aux = self._output_value.get()
        self._input_value.set(self.remove_zero_on_the_right(aux))

    def erase_last(self):
        """
            Erases last character from input box

            :param: nothing
            :return: nothing
        """
        if not self._input_value.get()[0] == 'I':
            input_value = self._input_value.get()[:-1]
            if input_value == '':
                self._input_value.set(self._DEFAULT_EMPTY_INPUT)
            else:
                self._input_value.set(input_value)
                if self.represents_int(input_value[-1]) and (input_value.count('(') == input_value.count(')')):
                    self.trigger_event(self._input_value.get())

    def clear(self):
        """
            Clears both input and output boxes

            :param: nothing
            :return: nothing
        """
        self._input_value.set(self._DEFAULT_EMPTY_INPUT)
        self._output_value.set(0)

    def create_button(self, target, custom_props):
        """
            Creates and inserts a new button in canvas

            :param target: target canvas
            :param custom_props: object with button properties, each key value pair will override the
                    original key value pair from the default configuration button
            :return: nothing
        """
        props = self._DEFAULT_BUTTON_PROPS.copy()
        for k, v in custom_props.items():
            props[k] = v

        temp_button = Button(target, text=props['text'], fg=props['fg'], bg=props['bg'], command=props['command'],
                             height=props['height'], width=props['width'], font=props['font'])
        temp_button.grid(row=props['row'], column=props['column'],
                         columnspan=props['columnspan'], rowspan=props['rowspan'])

    def create_entry(self, target, text_variable):
        """
            Creates an Entry box

            :param target: target canvas
            :param text_variable: reference of text variable to use
            :return: nothing
        """
        entry = Entry(target, textvariable=text_variable, font=self._ENTRY_POINT, justify='right',
                      disabledforeground="dark green", disabledbackground="black", state="disabled")
        entry.grid(columnspan=4, ipadx=36, ipady=10)
        return entry

    def create_gui(self):
        """
            Create GUI window and all its boxes and buttons

            :param: no params
            :return: nothing
        """

        gui = Tk()
        gui.title("Calculadora - Qualidade de Software")

        # Prepare input and output fields
        self._input_value = StringVar()
        self._input_value.set(self._DEFAULT_EMPTY_INPUT)
        self._output_value = DoubleVar()
        self._output_value.set(0)
        self.create_entry(gui, self._input_value)
        self.create_entry(gui, self._output_value)

        # Create numerical buttons
        # Grid row and column position for numerical buttons
        row_positions = [5, 2, 2, 2, 3, 3, 3, 4, 4, 4]
        column_positions = [0, 0, 1, 2, 0, 1, 2, 0, 1, 2]
        for i in range(0, 10):
            self.create_button(gui, {'text': i, 'command': lambda x=i: self.press(x, run=True),
                                     'row': row_positions[i], 'column': column_positions[i]})

        # Create special buttons
        none_numerical_buttons_props = [
            {'text': '.', 'command': lambda: self.press('.'), 'row': 5, 'column': 1},
            {'text': '(', 'command': lambda: self.press('('), 'row': 6, 'column': 0},
            {'text': ')', 'command': lambda: self.press(')', run=True), 'row': 6, 'column': 1},
            {'text': ui_names['btn_clear'], 'command': lambda: self.clear(), 'row': 6, 'column': 2},
            {'text': '+', 'command': lambda: self.press('+'), 'row': 2, 'column': 3},
            {'text': '-', 'command': lambda: self.press('-'), 'row': 3, 'column': 3},
            {'text': 'x', 'command': lambda: self.press('*'), 'row': 4, 'column': 3},
            {'text': '/', 'command': lambda: self.press('/'), 'row': 5, 'column': 3},
            {'text': ui_names['btn_save'], 'command': lambda: self.save_result(), 'row': 6, 'column': 3},
            {'text': ui_names['btn_erase'], 'command': lambda: self.erase_last(), 'row': 5, 'column': 2}
        ]
        for button_props in none_numerical_buttons_props:
            self.create_button(gui, button_props)

        # Start interface
        gui.mainloop()
