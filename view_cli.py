"""view_cli.py - View para linha de comandos do padrão MVC para aplicações desktop"""

from view_interface import CalcViewInterface


class Cli(CalcViewInterface):
    def __init__(self):
        """
            Construct a new 'Gli' object.

            :param: no params
            :return: returns nothing
        """
        super().__init__('Gui')

    # Method override
    def set_triggers(self, controller):
        """
        Will not trigger events, only log to the command line

        :param controller:
        :return:
        """
        ...

    # Method override
    def initialize(self):
        """
            Initialize the GUI window

            :param: none
            :return: returns nothing
        """
        print("\n\n***************************************")
        print("***   Command line logging system   ***")

    # Method override
    def update(self, message):
        """
            Observer method override: Sets the output box with the result from the model(Observable)

            :param message: {"success": bool, "expression": string, "result": string}: Object from Observable object.
            :return: returns nothing
        """
        if message['success']:
            print('> {} = {}'.format(message["expression"], message["result"]))
        elif message['error']:
            self.show_error(message['error_title'], message['error_message'])

    @classmethod
    def show_error(cls, error_title, error_message):
        """
            Shows message box with the error_message

            :param: error_message
            :return: returns nothing
        """
        print("{}> {}".format(error_title, error_message))
