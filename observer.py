"""observer.py - Implementação do padrão de software Observer"""

class Observer:
    def __init__(self, name):
        """
            Construct a new 'Observer' object. Represents the subscriber.

            :param name: name reference of the observer instance
            :return: returns nothing
        """
        self.name = name

    def update(self, message):
        """
            Action to do with the message from the observable
            This method should be overridden

            :param message: message to pass to the observer
            :return: returns string
        """
        print('{} got message "{}"'.format(self.name, message))


class Observable:
    def __init__(self):
        """
            Construct a new 'Observable' object. Represents the publisher.

            :param: nothing
            :return: returns nothing
        """
        self.observers = set()

    def register(self, observer):
        """
            Registers a new observer.

            :param observer: instance object must extend Observer
            :return: returns string
        """
        self.observers.add(observer)

    def notify(self, message):
        """
            Send notification to all observers

            :param message: message to pass to the observer
            :return: returns string
        """
        for observer in self.observers:
            observer.update(message)

