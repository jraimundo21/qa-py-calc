"""controller.py - Controlador do padrão MVC para aplicações desktop"""

from exceptions import *
import json

with open('message_dictionary.json', encoding='utf-8') as f:
    my_dictionary = json.load(f)["pt"]


class Controller:
    def __init__(self, model, views):
        """
            Construct a new 'Controller' object.

            :param model: model instance
            :param views: An array of view instances
            :return: returns nothing
        """
        self._model = model
        self._views = views

    def launch(self):
        """
            Launch the controller chain reaction

            :param: no params
            :return: nothing
        """
        # Register views as observer of the model and initialize them
        for view in self._views:
            view.set_triggers(self)
            self._model.register(view)
            view.initialize()

    def compute_result(self, expression):
        """
            Send input expression for calculation

            :param: expression
            :return: nothing
        """
        try:
            self._model.compute(expression)
        except DivisionByZeroException as e:
            for view in self._views:
                view.show_error(my_dictionary["messageBoxes"]["warning"], e)
        except InvalidExpressionException as e:
            for view in self._views:
                view.show_error(my_dictionary["messageBoxes"]["warning"], e)

