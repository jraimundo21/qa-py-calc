"""view_interface.py - Interface para implementar nas Views"""

from observer import Observer
from abc import abstractmethod


class CalcViewInterface(Observer):

    @abstractmethod
    def set_triggers(self, controller):
        """
            Configures controller computation action

            :param: controller instance
            :return: returns nothing
        """
        ...

    @abstractmethod
    def initialize(self):
        """
            Initialize the interface instance

            :param: none
            :return: returns nothing
        """
        ...

    @abstractmethod
    def show_error(self, error_title, error_message):
        ...
